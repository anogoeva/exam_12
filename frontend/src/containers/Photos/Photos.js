import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useLocation} from "react-router-dom";
import {CircularProgress, Grid} from "@material-ui/core";
import PhotoItem from "../../components/PhotoItem/PhotoItem";
import PhotoLayout from "../../components/UI/Layout/PhotoLayout";
import {fetchPhotos} from "../../store/actions/photosActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {historyPush} from "../../store/actions/historyActions";

const Photos = () => {
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);
    const fetchLoading = useSelector(state => state.photos.fetchLoading);
    const user = useSelector(state => state.users.user);
    const search = useLocation().search;
    const loading = useSelector(state => state.users.loginLoading);

    useEffect(() => {
        dispatch(fetchPhotos(search));
    }, [dispatch, search]);

    const addPhoto = () => {
        dispatch(historyPush('/addPhoto'));
    };

    return photos && (
        <PhotoLayout>
            <Grid container direction="column" spacing={2}>
                <Grid item container justifyContent="space-between" alignItems="center">

                    {user ?
                        <ButtonWithProgress
                            onClick={addPhoto}
                            type="submit"
                            variant="contained"
                            color="secondary"
                            loading={loading}
                            disabled={loading}
                        >
                            Add photo
                        </ButtonWithProgress> :
                        ''}

                </Grid>
                <Grid item>
                    <Grid item container justifyContent="center" direction="row" spacing={1}>
                        {fetchLoading ? (
                            <Grid container justifyContent="center" alignItems="center">
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : photos.map(photo => (
                            <PhotoItem
                                key={photo._id}
                                id={photo._id}
                                title={photo.title}
                                image={photo.image}
                                user={photo.user}
                            />
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        </PhotoLayout>
    );
};

export default Photos;