import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Typography} from "@material-ui/core";
import PhotoForm from "../../components/PhotoForm/PhotoForm";
import {addedPhoto} from "../../store/actions/photosActions";

const NewPhoto = () => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.photos.addedPhotoError);
    const loading = useSelector(state => state.photos.addedPhotoLoading);

    const onSubmit = photoData => {
        dispatch(addedPhoto(photoData));
    };

    return (
        <>
            <Typography variant="h4">Add new photo</Typography>
            <PhotoForm
                onSubmit={onSubmit}
                error={error}
                loading={loading}
            />
        </>
    );
};

export default NewPhoto;