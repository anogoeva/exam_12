import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deletePhoto, fetchPhoto} from "../../store/actions/photosActions";
import {Box, Button, Paper, Typography} from "@material-ui/core";
import {apiURL} from "../../config";

const Photo = ({match}) => {

    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photo);
    const user = useSelector(state => state.users.user);
    const userDiff = photos ? photos : '';

    useEffect(() => {
        dispatch(fetchPhoto(match.params.id));
    }, [dispatch, match.params.id]);

    const userOne = user ? user._id : ''
    const userTwo = userDiff && userDiff[0] && userDiff[0].user._id ? userDiff[0].user._id : '';

    const deleteHandle = (photoId) => {
        dispatch(deletePhoto(photoId));
    };

    return photos && (
        <Paper component={Box} p={2}>
            <Typography
                variant="h4">{userTwo !== '' ? photos[0].user.displayName : user.displayName + ' doesn\'t have photos'}'s
                Gallery</Typography>
            {photos.map(photo => (
                <div key={photo._id}>
                    <Typography variant="h6">{photo.title}</Typography>
                    <img src={apiURL + '/' + photo.image} alt={photo.title} width="300" height="200"/>
                    {userOne === userTwo ?
                        <p>
                            <Button variant="outlined" color="secondary" aria-controls="simple-menu"
                                    aria-haspopup="true"
                                    onClick={() => deleteHandle(photo._id)}
                            >Delete</Button>
                        </p>
                        : ''}
                </div>
            ))}
        </Paper>
    );
};

export default Photo;