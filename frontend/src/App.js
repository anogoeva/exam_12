import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Photos from "./containers/Photos/Photos";
import NewPhoto from "./containers/NewPhoto/NewPhoto";
import Photo from "./containers/Photo/Photo";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Photos}/>
            <Route path="/personal=true" component={Photos}/>
            <Route path="/users/:id" component={Photo}/>
            <Route path="/addPhoto" component={NewPhoto}/>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
        </Switch>
    </Layout>
);

export default App;
