import React from 'react';
import {Button} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersActions";
import anonymous from '../../../../assets/images/anonymous.png';
import {Link} from "react-router-dom";

const UserMenu = ({user}) => {
    const dispatch = useDispatch();

    let cardImage = anonymous;

    if (user.picture) {
        cardImage = user.picture;
    }

    return (
        <>
            <Button aria-controls="simple-menu" aria-haspopup="true" color="inherit" component={Link}
                    to={'/users/' + user._id}>
                <img referrerPolicy="no-referrer" src={cardImage} alt="avatar" width="25" height="25"/>
                Hello, {user.displayName}
            </Button>
            <Button onClick={() => dispatch(logoutUser())} color="inherit">Logout</Button>
        </>
    );
};

export default UserMenu;