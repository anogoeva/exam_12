import {
    Box,
    Button,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
    Modal,
} from "@material-ui/core";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import React from "react";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    },
    style: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    },
    button: {
        color: 'white',
        border: '2px solid #fff'
    }
});

const PhotoItem = ({id, title, image, user}) => {

    const classes = useStyles();

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/' + image;
    }

    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <Grid item xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia

                    image={cardImage}
                    onClick={handleOpen}
                    title={title}
                    className={classes.media}
                />
                <CardContent><br/>

                </CardContent>
                <CardActions>
                    <IconButton component={Link} to={'/users/' + user._id}>
                        By: {user.displayName}
                    </IconButton>
                </CardActions>

                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box className={classes.style}>
                        <CardMedia
                            image={cardImage}
                            title={title}
                            className={classes.media}
                        />
                        <Button className={classes.button} variant="outlined" color="default"
                                aria-controls="simple-menu" aria-haspopup="true"
                                onClick={handleClose}
                        ><b>X</b></Button>
                    </Box>

                </Modal>
            </Card>
        </Grid>
    );
};

PhotoItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,

};

export default PhotoItem;