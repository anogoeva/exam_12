import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';
import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_PHOTOS_REQUEST = 'FETCH_PHOTOS_REQUEST';
export const FETCH_PHOTOS_SUCCESS = 'FETCH_PHOTOS_SUCCESS';
export const FETCH_PHOTOS_FAILURE = 'FETCH_PHOTOS_FAILURE';

export const FETCH_PHOTO_REQUEST = 'FETCH_PHOTO_REQUEST';
export const FETCH_PHOTO_SUCCESS = 'FETCH_PHOTO_SUCCESS';
export const FETCH_PHOTO_FAILURE = 'FETCH_PHOTO_FAILURE';

export const ADDED_PHOTO_REQUEST = 'ADDED_PHOTO_REQUEST';
export const ADDED_PHOTO_SUCCESS = 'ADDED_PHOTO_SUCCESS';
export const ADDED_PHOTO_FAILURE = 'ADDED_PHOTO_FAILURE';

export const DELETE_PHOTO_REQUEST = 'DELETE_PHOTO_REQUEST';
export const DELETE_PHOTO_SUCCESS = 'DELETE_PHOTO_SUCCESS';
export const DELETE_PHOTO_FAILURE = 'DELETE_PHOTO_FAILURE';

export const CLEAR_ERROR_PHOTO = 'CLEAR_ERROR_PHOTO';

export const fetchPhotosRequest = () => ({type: FETCH_PHOTOS_REQUEST});
export const fetchPhotosSuccess = photos => ({type: FETCH_PHOTOS_SUCCESS, payload: photos});
export const fetchPhotosFailure = () => ({type: FETCH_PHOTOS_FAILURE});

export const fetchPhotoRequest = () => ({type: FETCH_PHOTO_REQUEST});
export const fetchPhotoSuccess = photo => ({type: FETCH_PHOTO_SUCCESS, payload: photo});
export const fetchPhotoFailure = () => ({type: FETCH_PHOTO_FAILURE});

export const addedPhotoRequest = () => ({type: ADDED_PHOTO_REQUEST});
export const addedPhotoSuccess = () => ({type: ADDED_PHOTO_SUCCESS});
export const addedPhotoFailure = (error) => ({type: ADDED_PHOTO_FAILURE, payload: error});

export const deletePhotoRequest = () => ({type: DELETE_PHOTO_REQUEST});
export const deletePhotoSuccess = (id) => ({type: DELETE_PHOTO_SUCCESS, payload: id});
export const deletePhotoFailure = () => ({type: DELETE_PHOTO_FAILURE});

export const clearErrorPhoto = () => ({type: CLEAR_ERROR_PHOTO});

export const fetchPhotos = (query) => {
    return async (dispatch) => {
        try {
            dispatch(fetchPhotosRequest());
            const response = await axiosApi.get('/photos' + query);
            dispatch(fetchPhotosSuccess(response.data));
        } catch (error) {
            dispatch(fetchPhotosFailure());
            toast.error('Could not fetch photos!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};

export const fetchPhoto = id => {
    return async dispatch => {
        try {
            dispatch(fetchPhotoRequest());
            const response = await axiosApi.get('http://localhost:8000/photos/users/' + id);
            dispatch(fetchPhotoSuccess(response.data));
        } catch (e) {
            dispatch(fetchPhotoFailure());
        }
    };
};

export const addedPhoto = photoData => {
    return async dispatch => {
        try {
            dispatch(addedPhotoRequest());
            await axiosApi.post('/photos', photoData);
            dispatch(addedPhotoSuccess());
            dispatch(historyPush('/'));
            toast.success('Photo added');
        } catch (e) {
            dispatch(addedPhotoFailure(e.response.data));
            toast.error('Could not added photo');
        }
    };
};

export const deletePhoto = id => {
    return async (dispatch) => {
        try {
            dispatch(deletePhotoRequest());
            await axiosApi.delete(`/photos/${id}`);
            dispatch(deletePhotoSuccess(id));
            dispatch(historyPush('/'));
            toast.success('Successfully deleted photo with id - ' + id);
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('You don\'t have permission for this operation');
            }
            dispatch(deletePhotoFailure(error));
        }
    };
};