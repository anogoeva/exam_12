import {
    ADDED_PHOTO_FAILURE,
    ADDED_PHOTO_REQUEST,
    ADDED_PHOTO_SUCCESS,
    CLEAR_ERROR_PHOTO,
    DELETE_PHOTO_FAILURE,
    DELETE_PHOTO_REQUEST,
    DELETE_PHOTO_SUCCESS,
    FETCH_PHOTO_FAILURE,
    FETCH_PHOTO_REQUEST,
    FETCH_PHOTO_SUCCESS,
    FETCH_PHOTOS_FAILURE,
    FETCH_PHOTOS_REQUEST,
    FETCH_PHOTOS_SUCCESS
} from "../actions/photosActions";

const initialState = {
    photos: [],
    photo: null,
    fetchLoading: false,
    singleLoading: false,
    addedPhotoLoading: false,
    addedPhotoError: null
};

const photosReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PHOTOS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_PHOTOS_SUCCESS:
            return {...state, fetchLoading: false, photos: action.payload};
        case FETCH_PHOTOS_FAILURE:
            return {...state, fetchLoading: false};
        case FETCH_PHOTO_REQUEST:
            return {...state, singleLoading: true};
        case FETCH_PHOTO_SUCCESS:
            return {...state, singleLoading: false, photo: action.payload};
        case FETCH_PHOTO_FAILURE:
            return {...state, singleLoading: false};
        case ADDED_PHOTO_REQUEST:
            return {...state, addedPhotoLoading: true};
        case ADDED_PHOTO_SUCCESS:
            return {...state, addedPhotoLoading: false, addedPhotoError: null};
        case ADDED_PHOTO_FAILURE:
            return {...state, addedPhotoLoading: false, addedPhotoError: action.payload};
        case DELETE_PHOTO_REQUEST:
            return {...state, singleLoading: true};
        case DELETE_PHOTO_SUCCESS:
            const newState = state.photos.filter(val => val._id !== action.payload);
            return {...state, singleLoading: false, addedPhotoError: null, photos: newState};
        case DELETE_PHOTO_FAILURE:
            return {...state, singleLoading: false};
        case CLEAR_ERROR_PHOTO:
            return {...state, addedPhotoError: null};
        default:
            return state;
    }
};

export default photosReducer;