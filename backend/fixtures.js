const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Photo = require("./models/Photo");
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [admin, user] = await User.create(
    {
      username: 'admin',
      password: '123',
      token: nanoid(),
      email: 'admin@admin.com',
      displayName: 'super admin',
    }, {
      username: 'user',
      password: '123',
      token: nanoid(),
      email: 'user@user.com',
      displayName: 'super user',
    }
  );

  await Photo.create({
      user: admin,
      title: 'nature',
      image: 'fixtures/nature.png'
    }, {
      user: admin,
      title: 'life',
      image: 'fixtures/life.png'
    },
    {
      user: user,
      title: 'science',
      image: 'fixtures/science.png'
    },
    {
      user: user,
      title: 'medicine',
      image: 'fixtures/medicine.png'
    });

  await mongoose.connection.close();
};

run().catch(console.error);