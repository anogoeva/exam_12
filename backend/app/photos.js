const express = require('express');
const multer = require('multer');
const Photo = require('../models/Photo');
const config = require('../config');
const {nanoid} = require("nanoid");
const User = require("../models/User");
const auth = require("../middleware/auth");
const path = require("path");
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage})

router.get('/', async (req, res) => {
  try {
    const photos = await Photo.find().populate('user', 'displayName');
    res.send(photos);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/users/:id', async (req, res) => {
  try {
    const photos = await Photo.find({user: req.params.id}).populate('user', 'displayName');
    res.send(photos);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const photo = await Photo.findById(req.params.id);

    if (photo) {
      res.send(photo);
    } else {
      res.status(404).send({error: 'Photo not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const token = req.get('Authorization');
    const user = await User.findOne({token});
    const photoData = {
      title: req.body.title,
      user: user._id,
    };

    if (req.file) {
      photoData.image = 'uploads/' + req.file.filename;
    }

    const photo = new Photo(photoData);
    await photo.save();
    res.send(photo);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const photo = await Photo.findByIdAndRemove(req.params.id);

    if (photo) {
      res.send(`Photo '${photo.title} removed'`);
    } else {
      res.status(404).send({error: 'Photo not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;